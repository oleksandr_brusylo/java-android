package com.hleedua.galaktika_kino;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class MoviesFragment extends Fragment {

    ArrayList<Movie> moviesList = new ArrayList<>();
    GridView gridView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                                                        Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.movie_fragment, container, false);
        gridView = (GridView) rootView.findViewById(R.id.gridView);
        gridView.setNumColumns(GridView.AUTO_FIT);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.putExtra("detailsUrl", moviesList.get(position).getDetailsPage());
                startActivity(intent);
            }
        });
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

            ConnectivityManager connMgr = (ConnectivityManager)
                    getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                new GetData().execute(getArguments().getString("url"));
            } else {
                Toast.makeText(getActivity(), "Отсутствует интернет подключение", Toast.LENGTH_LONG)
                                                                                            .show();
            }

    }

    class GetData extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {

            try {
                Document webPage = Jsoup.connect(params[0]).get();
                Elements elements = webPage.getElementsByClass("post");
                Movie movie;
                for (Element e : elements) {
                    movie = new Movie();
                    movie.setMovieTitle(e.select("h2").text());
                    movie.setMoviePicture(e.select("img").attr("abs:src"));
                    movie.setDetailsPage(e.select("a[href]").attr("abs:href"));
                    moviesList.add(movie);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            MoviesAdapter adapter = new MoviesAdapter(getActivity(), moviesList);
            gridView.setAdapter(adapter);
        }
    }
}
