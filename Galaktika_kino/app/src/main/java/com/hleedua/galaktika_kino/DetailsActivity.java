package com.hleedua.galaktika_kino;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class DetailsActivity extends ActionBarActivity {

    String[] headers;
    String moviePosterUrl;
    String movieTrailerUrl;
    String movieDescription;
    String movieTitle;
    String movieDetailsUrl;

    private ActionBarDrawerToggle drawerToggle;
    private CharSequence drawerTitle;
    private CharSequence appTitle;
    ListView drawerList;
    DrawerLayout drawerLayout;
    String[] drawerItems;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_list);

        movieDetailsUrl = getIntent().getStringExtra("detailsUrl");
        new GetData().execute(movieDetailsUrl);

        FragmentManager manager = getSupportFragmentManager();
        Fragment detailsFragment = manager.findFragmentById(R.id.container);
        if (detailsFragment == null) {
            detailsFragment = new DetailsFragment();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.container, detailsFragment);
            transaction.commit();
        }

        initializeDrawer();
        checkIfFirstStart();
    }

    private void initializeDrawer() {
        appTitle = getTitle();
        drawerTitle = getResources().getString(R.string.menu);
        drawerItems = getResources().getStringArray(R.array.drawer_items);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerList = (ListView) findViewById(R.id.left_drawer);
        drawerList.setAdapter(new ArrayAdapter<>(this, R.layout.drawer_list_item, drawerItems));
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer,
                R.drawable.ic_drawer
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(appTitle);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(drawerTitle);
                invalidateOptionsMenu();
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item);
    }

    @Override
    public void setTitle(CharSequence title) {
        appTitle = title;
        getSupportActionBar().setTitle(appTitle);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    private void setData() {
        ImageView detailMoviePicture = (ImageView) findViewById(R.id.detailMoviePicture);
        TextView detailMovieTitle = (TextView) findViewById(R.id.detailMovieTitle);
        TextView hallNumber = (TextView) findViewById(R.id.hallNumber);
        TextView sessions = (TextView) findViewById(R.id.sessions);
        TextView detailMovieGenre = (TextView) findViewById(R.id.detailMovieGenre);
        TextView movieDirector = (TextView) findViewById(R.id.movieDirector);
        TextView movieActors = (TextView) findViewById(R.id.movieActors);
        TextView movieProduction = (TextView) findViewById(R.id.movieProduction);
        TextView movieDuration = (TextView) findViewById(R.id.movieDuration);
        TextView movieDescription = (TextView) findViewById(R.id.movieDescription);

        Picasso.with(this).load(moviePosterUrl).into(detailMoviePicture);
        detailMovieTitle.setText(movieTitle);
        hallNumber.setText(headers[0]);
        sessions.setText(headers[1]);
        detailMovieGenre.setText(headers[2]);
        movieDirector.setText(headers[3]);
        movieActors.setText(headers[4]);
        movieProduction.setText(headers[5]);
        movieDuration.setText(headers[6]);
        movieDescription.setText(this.movieDescription);
    }

    public void checkIfFirstStart() {
        SharedPreferences sp = getSharedPreferences("appFirstStart", MODE_PRIVATE);
        boolean isFirstStart = sp.getBoolean("key", true);
        if(isFirstStart) {
            drawerLayout.openDrawer(drawerList);
            SharedPreferences.Editor e = sp.edit();
            e.putBoolean("key", false);
            e.apply();
        }
    }

    class GetData extends AsyncTask<String, Void, String[]> {

        @Override
        protected void onPreExecute() {
            headers=null;
            movieTrailerUrl = null;
        }

        @Override
        protected String[] doInBackground(String... params) {
            try {
                Document webPage = Jsoup.connect(params[0]).get();
                Element element = webPage.getElementById("film");
                movieTrailerUrl = element.getElementById("yout").child(0).attr("src");
                movieTitle = element.select("h1").text();
                moviePosterUrl = element.select("img").attr("abs:src");
                movieDescription = element.getElementsByTag("p").first().text();
                Element param = element.getElementById("param");
                Elements paramChildren = param.getElementsByClass("opi");
                headers = new String[paramChildren.size()];
                int headerNum=0;
                for (Element e : paramChildren) {
                    headers[headerNum] = e.text();
                    headerNum++;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return headers;
        }

        @Override
        protected void onPostExecute(String[] titles) {
            if (titles==null){
                Toast.makeText(DetailsActivity.this, "Отсутствует интернет подключение",
                                                                          Toast.LENGTH_LONG).show();
            } else {
                drawerList.setOnItemClickListener(new DrawerItemClickListener(DetailsActivity.this,
                               drawerLayout, drawerList, drawerItems, movieTrailerUrl, movieDetailsUrl));
                setData();
            }
        }
    }
}
