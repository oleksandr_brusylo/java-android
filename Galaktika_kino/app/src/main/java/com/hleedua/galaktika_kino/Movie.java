package com.hleedua.galaktika_kino;

public class Movie {

    private String movieTitle;
    private String moviePicture;
    private String detailsPage;

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getMoviePicture() {
        return moviePicture;
    }

    public void setMoviePicture(String moviePicture) {
        this.moviePicture = moviePicture;
    }

    public String getDetailsPage() {
        return detailsPage;
    }

    public void setDetailsPage(String detailsPage) {
        this.detailsPage = detailsPage;
    }

}
