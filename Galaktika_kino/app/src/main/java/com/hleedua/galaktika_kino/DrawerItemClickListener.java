package com.hleedua.galaktika_kino;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

class DrawerItemClickListener implements ListView.OnItemClickListener {

    DetailsActivity context;
    String trailerUrl;
    String detailsUrl;
    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private String[] drawerItems;

    public DrawerItemClickListener(DetailsActivity context, DrawerLayout layout, ListView list,
                                            String [] items, String trailerUrl, String detailsUrl) {
        this.context = context;
        this.drawerLayout = layout;
        this.drawerList = list;
        this.drawerItems = items;
        this.detailsUrl = detailsUrl;
        this.trailerUrl = trailerUrl;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                context.setTitle(drawerItems[position]);
                drawerLayout.closeDrawer(drawerList);
                fragment = new SchemesFragment();
                break;
            case 1:
                if (((TelephonyManager) context.
                        getSystemService(Context.TELEPHONY_SERVICE)).getLine1Number() == null) {
                    Toast.makeText(context,
                            "Устройство не позволяет совершать звонки", Toast.LENGTH_SHORT).show();
                } else {
                    String number = context.getResources().getString(R.string.phone_number_info);
                    Intent callIntent = new Intent(Intent.ACTION_CALL, Uri
                                                                           .parse("tel:" + number));
                    context.startActivity(callIntent);
                }
                break;
            case 2:
                if (detailsUrl == null) {
                    Toast.makeText(context, "Ошибка сети", Toast.LENGTH_SHORT).show();
                } else {
                    Intent sendLink = new Intent(Intent.ACTION_SEND);
                    sendLink.setType("text/plain");
                    sendLink.putExtra(Intent.EXTRA_TEXT, "Идем смотреть ? " + "\n" + detailsUrl);
                    context.startActivity(sendLink);
                }
                break;
            case 3:
                if (trailerUrl == null) {
                    Toast.makeText(context, "Трейлер не найден", Toast.LENGTH_SHORT).show();
                } else {
                    Intent watchTrailer = new Intent(Intent.ACTION_VIEW, Uri.parse(trailerUrl));
                    context.startActivity(watchTrailer);
                }
                break;
            case 4:
                context.setTitle(drawerItems[position]);
                drawerLayout.closeDrawer(drawerList);
                fragment = new AboutFragment();
                break;
            case 5:
                Intent displayMap = new Intent(Intent.ACTION_VIEW);
                displayMap.setData(Uri.parse(context.getString(R.string.geo_location_info)));
                if (displayMap.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivity(displayMap);
                } else {
                    Toast.makeText(context, "Невозможно отобразить", Toast.LENGTH_SHORT).show();
                }
                break;
            case 6:
                String number = context.getResources().getString(R.string.phone_number_info);
                String address = context.getResources().getString(R.string.address_info);
                showContactsDialog(number, address);
                break;
            default:
                break;
        }

        drawerList.setItemChecked(position, true);
        //drawerList.setSelection(position);

        if (fragment != null) {
            android.support.v4.app.FragmentTransaction transaction = context
                                                    .getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, fragment);
            transaction.commit();
        } else {
            Log.e("tag", "Fragment wasn't initialized");
        }
    }

    void showContactsDialog(String number, String address) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(address + "\n" + number);
        builder.setNeutralButton("OК", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
}
