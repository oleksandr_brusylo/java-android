package com.hleedua.galaktika_kino;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class SchemesFragment extends Fragment implements View.OnClickListener {

    ImageView ivHallPicture;
    TextView tvHallTitle;
    Button btnHallOne;
    Button btnHallTwo;
    Button btnHallThree;
    Button btnHallFour;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.halls_schemes, container, false);
        ivHallPicture = (ImageView) rootView.findViewById(R.id.ivHallPicture);
        tvHallTitle = (TextView) rootView.findViewById(R.id.tvHallTitle);
        btnHallOne = (Button) rootView.findViewById(R.id.btnHallOne);
        btnHallOne.setOnClickListener(this);
        btnHallTwo = (Button) rootView.findViewById(R.id.btnHallTwo);
        btnHallTwo.setOnClickListener(this);
        btnHallThree = (Button) rootView.findViewById(R.id.btnHallThree);
        btnHallThree.setOnClickListener(this);
        btnHallFour = (Button) rootView.findViewById(R.id.btnHallFour);
        btnHallFour.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        int pressedButton = v.getId();
        switch (pressedButton) {
            case R.id.btnHallOne:
                tvHallTitle.setText(getString(R.string.hallOne));
                ivHallPicture.setImageResource(R.drawable.hall1);
                break;
            case R.id.btnHallTwo:
                tvHallTitle.setText(getString(R.string.hallTwo));
                ivHallPicture.setImageResource(R.drawable.hall2);
                break;
            case R.id.btnHallThree:
                tvHallTitle.setText(getString(R.string.hallThree));
                ivHallPicture.setImageResource(R.drawable.hall3);
                break;
            case R.id.btnHallFour:
                tvHallTitle.setText(getString(R.string.hallFour));
                ivHallPicture.setImageResource(R.drawable.hall4);
                break;
            default:
                break;
        }
    }
}
