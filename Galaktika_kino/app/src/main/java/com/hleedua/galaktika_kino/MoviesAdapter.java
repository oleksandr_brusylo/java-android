package com.hleedua.galaktika_kino;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MoviesAdapter extends ArrayAdapter<Movie> {

    Context context;
    ArrayList<Movie> moviesList;

    public MoviesAdapter(Context context, ArrayList<Movie> moviesList) {
        super(context, R.layout.custom_row, moviesList);
        this.context = context;
        this.moviesList = moviesList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View row = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context
                                                                          .LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.custom_row, parent, false);
            holder = new ViewHolder();
            holder.movieTitle = (TextView) row.findViewById(R.id.movieTitle);
            holder.moviePicture = (ImageView) row.findViewById(R.id.moviePicture);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }
        holder.movieTitle.setText(moviesList.get(position).getMovieTitle());
        Picasso.with(context).load(moviesList.get(position).getMoviePicture())
                                                                         .into(holder.moviePicture);
        return row;
    }

    static class ViewHolder{
        ImageView moviePicture;
        TextView movieTitle;
    }

}
