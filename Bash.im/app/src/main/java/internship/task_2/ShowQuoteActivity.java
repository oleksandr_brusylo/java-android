package internship.task_2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class ShowQuoteActivity extends Activity {

    String imageRef;
    boolean noComics;
    String quoteText;
    String quoteId;
    TextView textView;
    ImageView comics;
    public ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_quote);
        textView = (TextView) findViewById(R.id.bigText);

        if (intentFromCache()) {
            showCachedData();
        }
        else {

            quoteText = getIntent().getStringExtra(MainActivity.TEXT);
            quoteId = getIntent().getStringExtra(MainActivity.ID);
            setTitle("Цитата " + quoteId);
            noComics = getIntent().getStringExtra(MainActivity.PAGE_REF) == null;
            if (noComics) {
                textView.setText(quoteText);
            } else {
                if (MainActivity.demoVersion) {
                    textView.setText(quoteText);
                    new MainActivity().demoDialogConstraint(this);
                } else {
                    new ComicsLoader().execute();
                }
            }
        }
    }

    private boolean intentFromCache() {
        return getIntent().getStringExtra("IMG_REF_CACHE") != null ||
                                                         getIntent().getStringExtra("TEXT_CACHE") != null;
    }

    private void showCachedData() {
        setTitle("Цитата " + getIntent().getStringExtra("ID"));
        imageRef = getIntent().getStringExtra("IMG_REF_CACHE");
        quoteText = getIntent().getStringExtra("TEXT_CACHE");
        if (imageRef != null) {
            comics = (ImageView) findViewById(R.id.singleComics);
            Picasso.with(this).load(getIntent().getStringExtra("IMG_REF_CACHE")).into(comics);
        } else {
            noComics = true;
            textView.setText(quoteText);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_single_quote, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (connectionAvailable()) {
            if (item.getItemId() == R.id.share) {
                if (noComics || MainActivity.demoVersion) {
                    shareData("цитату", quoteText);
                } else {
                    shareData("комикс", imageRef);
                }
            }
            return true;
        }
        return false;
    }

    private boolean connectionAvailable() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            Toast.makeText(this, "Отсутствует интернет соединение", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private void shareData(String description, String content) {

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, "");
        intent.putExtra(Intent.EXTRA_TEXT, content);
        startActivity(Intent.createChooser(intent, "Отправить " + description));
    }

    /**
     * @param url is outer reference to page from which method fetches data
     * @return direct image (jpg, png, etc) reference
     */
    private String getImageReference(String url) {
        Document webPageComics;
        String imageRef = null;
        try {
            webPageComics = Jsoup.connect(url).get();
            imageRef = webPageComics.getElementById("cm_strip").attr("src");
        } catch (IOException e) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "Отсутствует интернет соединение",
                            Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                }
            });
        }
        return imageRef;
    }

    private class ComicsLoader extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ShowQuoteActivity.this);
            pd.setMessage("Загружаю комикс");
            pd.setCanceledOnTouchOutside(false);
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... arg) {
            imageRef = getImageReference(getIntent().getStringExtra(MainActivity.PAGE_REF));
            if (imageRef != null) {
                DBHelper dbh = new DBHelper(ShowQuoteActivity.this);
                dbh.setComicsReference(imageRef, quoteId);
                dbh.close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void arg) {
            super.onPostExecute(arg);
            comics = (ImageView) findViewById(R.id.singleComics);
            Picasso.with(ShowQuoteActivity.this).load(imageRef).into(comics, new Callback() {
                @Override
                public void onSuccess() {
                    pd.dismiss();
                }

                @Override
                public void onError() {
                    pd.dismiss();
                }
            });
        }
    }
}
