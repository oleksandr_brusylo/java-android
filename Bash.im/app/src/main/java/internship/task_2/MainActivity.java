package internship.task_2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends Activity {

    private MainAdapter mainAdapter;
    private ListView listView;
    private String url = "http://bash.im/";
    private String idQuery = "a[class=id]";
    public static final String TEXT = "TEXT";
    public static final String PAGE_REF = "PAGE_REF";
    public static final String ID = "ID";
    public static final String CASE = "CASE";
    private ArrayList<Quote> quotesList = new ArrayList<>();
    public static boolean demoVersion = BuildFlavor.VERSION == BuildFlavor.DEMO;
    static float fontSize;
    boolean enableCache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listView);
        new FetchThread().execute();
        mainAdapter = new MainAdapter(MainActivity.this, quotesList);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                handleClick(position);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        fontSize = Float.parseFloat(preferences.getString("fontSize", "12"));
        enableCache = preferences.getBoolean("cache", true);
//        mainAdapter = new MainAdapter(MainActivity.this, quotesList);
//        listView.setAdapter(mainAdapter);
    }

    private void handleClick(int position) {
        Intent intent = new Intent(this, ShowQuoteActivity.class);
        intent.putExtra(ID, quotesList.get(position).getId());
        intent.putExtra(TEXT, quotesList.get(position).getText());
        intent.putExtra(PAGE_REF, quotesList.get(position).getComicsPageRef());
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void changeTab(String url, String query, String title) {
        this.url = url;
        mainAdapter.validateUrl(url);
        idQuery = query;
        new FetchThread().execute();
        setTitle(title);
    }

    private void setCacheCase(String cacheCase) {
        Intent intent = new Intent(this, CachedQuotesActivity.class);
        intent.putExtra(CASE, cacheCase);
        startActivity(intent);
    }

     void demoDialogConstraint(Context context) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("Невозможно отобразить");
        dialog.setMessage("Возможность просматривать комиксы недоступна в демо-версии" +
                " приложения");
        dialog.setNeutralButton("Назад", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.newQ:
                changeTab(getString(R.string.urlNew), "a[class=id]", getString(R.string.titleNew));
                break;
            case R.id.randomQ:
                changeTab(getString(R.string.urlRandom), "a[class=id]", getString(R.string.titleRandom));
                break;
            case R.id.bestQ:
                changeTab(getString(R.string.urlBest), "a[class=id]", getString(R.string.titleBest));
                break;
            case R.id.byRatingQ:
                changeTab(getString(R.string.urlByRating), "a[class=id]", getString(R.string.titleByRating));
                break;
            case R.id.abyssQ:
                changeTab(getString(R.string.urlAbyss), "span[class=id]", getString(R.string.titleAbyss));
                break;
            case R.id.abyssTopQ:
                changeTab(getString(R.string.urlAbyssTop), "span[class=abysstop]", getString(R.string.titleAbyssTop));
                break;
            case R.id.abyssBestQ:
                changeTab(getString(R.string.urlAbyssBest), "span[class=id]", getString(R.string.titleAbyssBest));
                break;
            case R.id.cachedQuotes:
                setCacheCase("TEXT");
                break;
            case R.id.cachedComics:
                if (demoVersion) {
                    demoDialogConstraint(this);
                    break;
                }
                setCacheCase("COMICS");
                break;
            case R.id.preferences:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    class FetchThread extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Загрузка...");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg) {
            Document webPage;
            String textQuery = ".text";
            String hrefQuery = ".actions a[href]";
            String comicsQuery = ".actions a[class=comics]";
            try {
                quotesList.clear();
                webPage = Jsoup.connect(url).get();
                Elements quotes = webPage.select(".quote");
                fetchData(textQuery, hrefQuery, comicsQuery, quotes);
                if (enableCache) {
                    writeQuotesToDB();
                }
            } catch (IOException e) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "Отсутствует интернет соединение",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void arg) {
            listView.setAdapter(mainAdapter);
            progressDialog.dismiss();
        }
    }

    /**
     * Fetches data from connected URL via Jsoup library. Depending on site section, idQuery may be
     * different from previous. Some elements doesn't contain any data (server side feature), that's
     * why there is checking idQuery for null.
     *
     * @param textQuery   fetches quote text from quote element
     * @param hrefQuery   fetches direct reference on current quote and on web page with comics
     * @param comicsQuery checks whether quote contains comics or not
     * @param quotes      all quote elements received from current web page
     */
    private void fetchData(String textQuery, String hrefQuery, String comicsQuery, Elements quotes) {
        Quote quote;
        boolean hasComics;
        String absHref = "abs:href";
        for (Element element : quotes) {
            if (element.select(idQuery).first() != null) {
                quote = new Quote();
                quote.setId(element.select(idQuery).first().text());
                quote.setText(getReadableText(element.select(textQuery).first()));
                hasComics = (element.select(comicsQuery).first() != null);
                quote.setContainsComics(hasComics);
                if (hasComics) {
                    quote.setComicsPageRef(element.select(hrefQuery).last().attr(absHref));
                }
                if (element.select(hrefQuery).first() != null) {
                    quote.setQuoteRef(element.select(hrefQuery).first().attr(absHref));
                }
                quotesList.add(quote);
            }
        }
    }

    private void writeQuotesToDB() {
        DBHelper dbh = new DBHelper(this);
        dbh.setQuotes(quotesList);
        dbh.close();
    }

    /**
     * Converts text into readable via replacing <br> with newline
     *
     * @param elem contains text with <br> children elements instead of newlines
     * @return formatted text
     */
    private String getReadableText(Element elem) {
        elem.children().prepend("uniqueValue");
        String formattedText = elem.text();
        return formattedText.replaceAll("uniqueValue", "\n");
    }

    public class VoteThread extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            //params[0] - contains either "rulez" for vote Up or "sux" for vote Down
            String quoteRef = params[1];
            String quoteId = params[2].substring(1);
            try {
                Jsoup.connect(quoteRef).data("act", params[0]).data("quote", quoteId)
                        .referrer(url).method(Connection.Method.POST).execute();

            } catch (IOException e) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "Отсутствует интернет соединение",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }
    }

    public class MainAdapter extends ArrayAdapter<Quote> {

        private String urlAbyss = getString(R.string.urlAbyss);
        private String urlAbyssTop = getString(R.string.urlAbyssTop);
        private String urlAbyssBest = getString(R.string.urlAbyssBest);
        Context context;
        boolean invalidUrl;

        public MainAdapter(Context context, ArrayList<Quote> quotes) {
            super(context, R.layout.single_row, quotes);
            this.context = context;
        }

        public void validateUrl(String currentUrl) {
            invalidUrl = currentUrl.equals(urlAbyss) ||
                                  currentUrl.equals(urlAbyssTop) || currentUrl.equals(urlAbyssBest);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            View row = convertView;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.single_row, parent, false);
                holder = new ViewHolder();
                holder.btnPlus = (Button) row.findViewById(R.id.voteRulez);
                holder.btnMinus = (Button) row.findViewById(R.id.voteSux);
                holder.quoteText = (TextView) row.findViewById(R.id.quoteText);
                holder.quoteId = (TextView) row.findViewById(R.id.quoteId);
                holder.quoteComics = (TextView) row.findViewById(R.id.quoteComics);
                holder.quoteText.setTextSize(fontSize);
                holder.quoteId.setTextSize(fontSize);
                holder.quoteComics.setTextSize(fontSize);
                row.setTag(holder);
            } else {
                holder = (ViewHolder) row.getTag();
            }

            if (invalidUrl) {                                  //no possibility to vote on this page
                holder.btnPlus.setVisibility(View.INVISIBLE);
                holder.btnMinus.setVisibility(View.INVISIBLE);
            }

            holder.btnPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    vote("rulez", position);
                }
            });
            holder.btnMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    vote("sux", position);
                }
            });
            holder.quoteText.setText(quotesList.get(position).getText());
            holder.quoteId.setText(quotesList.get(position).getId());
            if (quotesList.get(position).getContainsComics()) {
                holder.quoteComics.setText("К О М И К С");
            } else {
                holder.quoteComics.setText("");
            }
            return row;
        }
    }

    private void vote(String decision, int position) {
        new VoteThread().execute(decision, quotesList.get(position).getQuoteRef(),
                quotesList.get(position).getId());
    }

    static class ViewHolder {
        Button btnPlus;
        Button btnMinus;
        TextView quoteText;
        TextView quoteId;
        TextView quoteComics;
    }

}


