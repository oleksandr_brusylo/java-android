package internship.task_2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class CacheAdapter extends ArrayAdapter<Quote> {
    Context context;
    static String cacheCase;
    ArrayList<Quote> quotes = new ArrayList<>();

    public CacheAdapter(Context context, ArrayList<Quote> cachedQuotes) {
        super(context, R.layout.single_row_cached, cachedQuotes);
        this.context = context;
        quotes = cachedQuotes;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context
                                                 .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.single_row_cached, parent, false);
            holder = new ViewHolder();
            holder.quoteId = (TextView)row.findViewById(R.id.cachedQuoteId);
            holder.quoteText = (TextView)row.findViewById(R.id.cachedQuoteText);
            holder.quoteComics = (ImageView) row.findViewById(R.id.cachedComics);
            holder.quoteText.setTextSize(MainActivity.fontSize);
            holder.quoteId.setTextSize(MainActivity.fontSize);
            row.setTag(holder);
        } else {
            holder = (ViewHolder)row.getTag();
        }

        if (cacheCase.equals("TEXT")) {
            holder.quoteText.setText(quotes.get(position).getText());
            holder.quoteId.setText(quotes.get(position).getId());
        } else {
            holder.quoteId.setText("По мотивам цитаты " + quotes.get(position).getId());
            Picasso.with(context).load(quotes.get(position).getImgRef()).into(holder.quoteComics);
        }
        return row;
    }

    static class ViewHolder {
        TextView quoteId;
        TextView quoteText;
        ImageView quoteComics;
    }

}
