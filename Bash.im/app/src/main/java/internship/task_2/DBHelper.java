package internship.task_2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {

    public static final String TABLE_NAME = "quotes";
    public static final String ID = "_id";
    public static final String QUOTE_TEXT = "text";
    public static final String QUOTE_ID = "quoteId";
    public static final String COMICS_REF = "comicsRef";
    private static final String DATABASE_NAME = "quotesDB";
    private static final int DATABASE_VERSION = 1;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " ("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + QUOTE_TEXT + " TEXT,"
                + COMICS_REF + " TEXT UNIQUE,"
                + QUOTE_ID + " TEXT UNIQUE" + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        this.onCreate(db);
    }

    /**
     * Method inserts quote data into Database. Database contains only unique quotes,
     * same quotes will be skipped
     *
     * @param quotes contains required data to insert into Database
     */
    public void setQuotes(List<Quote> quotes) {
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            for (Quote quote : quotes) {
                cv.put(QUOTE_TEXT, quote.getText());
                cv.put(QUOTE_ID, quote.getId());
                try {
                    db.insertOrThrow(TABLE_NAME, null, cv);
                } catch (SQLiteConstraintException e) {
                    //this quote already saved, skipping
                }
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        db.close();
    }

    /**
     * @param reference is direct image url to insert into Database
     * @param quoteId   required to insert reference into appropriate row
     */
    public void setComicsReference(String reference, String quoteId) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("UPDATE " + TABLE_NAME + " SET " + COMICS_REF + " = '" + reference +
                "' WHERE " + QUOTE_ID + " = '" + quoteId + "'");
        db.close();
    }


    /**
     * Method selects all quotes from Database without specific parameters (selection,
     * selectionArgs, groupBy, having, orderBy), null instead;
     *
     * @return quotes readed from Database
     */
    public ArrayList<Quote> getQuotesFromDB() {
        ArrayList<Quote> quotesList = new ArrayList<>();
        Quote quote;
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.query(TABLE_NAME, new String[]{
                        ID, QUOTE_TEXT, COMICS_REF, QUOTE_ID},
                null,
                null,
                null,
                null,
                null
        );
        while (cursor.moveToNext()) {
            quote = new Quote();
            quote.setId(cursor.getString(cursor.getColumnIndex(QUOTE_ID)));
            quote.setText(cursor.getString(cursor.getColumnIndex(QUOTE_TEXT)));
            quote.setImgRef(cursor.getString(cursor.getColumnIndex(COMICS_REF)));
            quotesList.add(quote);
        }
        cursor.close();
        db.close();
        return quotesList;
    }

    public int clearTable() {
        SQLiteDatabase db = getWritableDatabase();
        int rowsDeleted = db.delete(TABLE_NAME, null, null);
        db.close();
        return rowsDeleted;
    }
}
