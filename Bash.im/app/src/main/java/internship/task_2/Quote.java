package internship.task_2;

public class Quote {

    private String id;
    private String text;
    private boolean containsComics;
    private String comicsPageRef;
    private String quoteRef;
    private String imgRef;

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public boolean getContainsComics() {
        return containsComics;
    }

    public String getComicsPageRef() {
        return comicsPageRef;
    }

    public String getQuoteRef() {
        return quoteRef;
    }

    public String getImgRef() {return imgRef;}

    public void setId(String id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setContainsComics(boolean containsComics) {
        this.containsComics = containsComics;
    }

    public void setComicsPageRef(String imageRef) {
        this.comicsPageRef = imageRef;
    }

    public void setQuoteRef(String quoteRef) {
        this.quoteRef = quoteRef;
    }

    public void setImgRef(String imgRef) {
        this.imgRef = imgRef;
    }

}
