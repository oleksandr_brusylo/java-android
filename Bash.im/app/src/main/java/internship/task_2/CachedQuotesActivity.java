package internship.task_2;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

public class CachedQuotesActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeAdapter(getIntent().getStringExtra(MainActivity.CASE));
    }

    private void initializeAdapter(String adapterCase) {

        DBHelper db = new DBHelper(this);
        ArrayList<Quote> cachedQuotes = db.getQuotesFromDB();
        db.close();
        ArrayList<Quote> cachedComics = getComics(cachedQuotes);
        CacheAdapter.cacheCase = adapterCase;

        if (cachedQuotes.size() == 0) {
            Toast.makeText(this, "Нет данных", Toast.LENGTH_SHORT).show();onBackPressed();
        } else {
            if (adapterCase.equals("TEXT")) {
                showCachedData(cachedQuotes);
            } else {
                if (cachedComics.size() == 0) {
                    Toast.makeText(this, "Нет данных", Toast.LENGTH_SHORT).show();onBackPressed();
                } else {
                    showCachedData(cachedComics);
                }
            }
        }
    }

    private void showCachedData(final ArrayList<Quote> cachedData) {
        CacheAdapter adapter = new CacheAdapter(this, cachedData);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(CachedQuotesActivity.this, ShowQuoteActivity.class);
                intent.putExtra("TEXT_CACHE", cachedData.get(position).getText());
                intent.putExtra("IMG_REF_CACHE", cachedData.get(position).getImgRef());
                intent.putExtra("ID", cachedData.get(position).getId());
                startActivity(intent);
            }
        });
    }

    /**
     * Method converts incoming quotes list into specific list that contains only comics
     *
     * @param cachedQuotes quotes from Database
     * @return list of quotes with comics
     */
    private ArrayList<Quote> getComics(ArrayList<Quote> cachedQuotes) {
        ArrayList<Quote> comicsOnly = new ArrayList<>();
        for (Quote withImg : cachedQuotes) {
            if (withImg.getImgRef() != null)
                comicsOnly.add(withImg);
        }
        return comicsOnly;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cached, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void clearCache() {

        File imagesDir =
              new File(getApplicationContext().getCacheDir().getAbsolutePath() + "/picasso-cache/");
        File[] cachedImages = imagesDir.listFiles();
        if (cachedImages != null) {
            for (File image : cachedImages) {
                image.delete();
            }
        }
        DBHelper dbh = new DBHelper(CachedQuotesActivity.this);
        dbh.clearTable();
        dbh.close();
            Toast.makeText(CachedQuotesActivity.this, "Данные успешно удалены",
                                                                         Toast.LENGTH_SHORT).show();
            onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clearCache:

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Удалить загруженные данные ?");
                builder.setCancelable(false);
                builder.setMessage
                        ("Загруженные цитаты можно просматривать без подключения к сети");
                builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clearCache();
                    }
                });
                builder.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
