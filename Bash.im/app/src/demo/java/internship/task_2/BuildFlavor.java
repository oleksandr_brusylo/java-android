package internship.task_2;


public enum BuildFlavor {

    DEMO, FULL;

    public static final BuildFlavor VERSION = BuildFlavor.DEMO;

}
