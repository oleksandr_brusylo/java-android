import org.junit.Test;
import project.TreeBuilder;
import project.FilesComparator;


import java.io.IOException;

public class TestClass {


    @Test(expected = IOException.class)
    public void testIncorrectUserInput() throws IOException {
        TreeBuilder builderInstance = new TreeBuilder("c;/games");
        builderInstance.buildFileTree();
        FilesComparator comparatorInstance = new FilesComparator(builderInstance.getFiles());
        comparatorInstance.startComparison();
    }

}
