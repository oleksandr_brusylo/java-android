package project;

import java.awt.*;
import java.io.IOException;
import java.util.*;

import static java.lang.System.*;

public class Main {

    public static void main(String[] args) {

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new GUI();
            }
        });


//        if (args.length > 0) {
//            processCommandLine(args);
//        } else {


//        }
    }

//    private static void processCommandLine(String[] args) {
//
//        FilesComparator comparator;
//        TreeBuilder builder;
//
//        String command = args[0];
//        if (command.equalsIgnoreCase("--help")) {
//            out.println("Enter directory to scan, for example 'C:\\Windows'");
//        } else {
//            try {
//                builder = new TreeBuilder(args[0]);
//                builder.buildFileTree();
//                comparator = new FilesComparator(builder.getFiles());
//                comparator.startComparison();
//            } catch (IOException e) {
//                out.println(e.getMessage());
//            }
//        }
//    }

     static void interactiveMode() {

        FilesComparator comparator;
        TreeBuilder builder;

        //out.println("Type 'Exit' to close program");

//        while (true) {
            try {
                String userInput = GUI.getRoot();
//                if (userInput.equalsIgnoreCase("Exit")) {
//                    out.println("Good bye");
//                    break;
//                }
                out.println("Starting...");

                builder = new TreeBuilder(userInput);
                builder.buildFileTree();

                comparator = new FilesComparator(builder.getFiles());
                comparator.startComparison();

                out.println("Done!");
            } catch (IOException e) {
                out.println(e.getMessage());
            }
        //}
    }

//    public static String acceptUserInput() {
//        Scanner sc = new Scanner(in);
//        return sc.nextLine();
//    }

}







