package project;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class DBWriter {

    private static String URL;
    private static String USER;
    private static String PASSWORD;
    private static String DRIVER_NAME;
    static PreparedStatement preparedStatement;
    static Connection dbConnection;
    static Statement statement;
    static Long scanId;

    static Connection getDBСonnection() {

        setProperties();
        setClass();

        try {
            dbConnection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException e) {
            System.out.println("Edit 'db.properties file'...");
            System.out.println(e.toString());
            System.exit(0);
        }
        return dbConnection;
    }

    static void setProperties() {

        Properties prop = new Properties();
        FileInputStream input = null;

        try {
            input = new FileInputStream("db.properties");
            prop.load(input);
            URL = prop.getProperty("url");
            USER = prop.getProperty("user");
            PASSWORD = prop.getProperty("pwd");
            DRIVER_NAME = prop.getProperty("driverName");

        } catch (IOException e) {
            System.out.println("File 'db.properties' not found");
            System.exit(0);
        } finally {
            try {
                if (input != null)
                    input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static void setClass() {

        try {
            Class.forName(DRIVER_NAME);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    static void createDuplicatesTable() {

        String createTableDuplicates = "CREATE TABLE IF NOT EXISTS Duplicates  " +
                "(FileId INT PRIMARY KEY AUTO_INCREMENT,  " +
                "FileLocation TEXT," +
                "ScanId LONG )";

        String insertDuplicates = "INSERT INTO  Duplicates  (FileLocation, ScanId)  VALUES (?,?) ";

        try {
            dbConnection = getDBСonnection();
            dbConnection.setAutoCommit(false);
            statement = dbConnection.createStatement();
            statement.execute(createTableDuplicates);
            preparedStatement = dbConnection.prepareStatement(insertDuplicates);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    static void closeConnection() {

        try {
            if (dbConnection != null) {
                dbConnection.close();
            }
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void createScanTable() {

        String createTableScans = "CREATE TABLE IF NOT EXISTS Scans" +
                "(ScanId INT PRIMARY KEY AUTO_INCREMENT ," +
                "ScanDate DATE)";

        String insertScanDate = "INSERT INTO Scans (ScanDate) VALUE (CURDATE())";

        try {
            dbConnection = getDBСonnection();
            //dbConnection.setAutoCommit(false);
            statement = dbConnection.createStatement();
            statement.execute(createTableScans);
            statement.execute(insertScanDate, Statement.RETURN_GENERATED_KEYS);
            //dbConnection.commit();

            ResultSet rs = DBWriter.statement.getGeneratedKeys();
            if (rs != null && rs.next()) {
                scanId = rs.getLong(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
