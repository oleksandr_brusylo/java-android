package project;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class GUI extends JFrame {

    static JTextArea textArea = new JTextArea();
    JPanel buttonsPanel = new JPanel();
    JScrollPane scrollPane = new JScrollPane(textArea);
    JButton aboutButton = new JButton("About");
    JButton scanButton = new JButton("Open");
    JButton exitButton = new JButton("Exit");
    JFileChooser fc = new JFileChooser("D:\\");
    static String root;


    public GUI() {

        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();
        int frameHeight = screenSize.height;
        int frameWidth = screenSize.width;
        setSize(frameWidth /2, frameHeight /2);
        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Delete Duplicates");
        Image icon = new ImageIcon("file_copy.png").getImage();
        setIconImage(icon);

        buttonsPanel.add(scanButton);
        buttonsPanel.add(aboutButton);
        buttonsPanel.add(exitButton);

        textArea.setEditable(false);


        add(buttonsPanel, BorderLayout.EAST);
        add(scrollPane);


        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

        scanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int returnVal = fc.showOpenDialog(GUI.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    root = file.getAbsolutePath();
                    scanButton.setEnabled(false);
                    compareConcurrently().start();
                    textArea.setText("");
                } else {
                    System.out.println("Open command cancelled by user.");
                }
            }
        });

        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(1);
            }
        });

    }



    public Thread compareConcurrently() {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                Main.interactiveMode();
                scanButton.setEnabled(true);
            }
        });
    }

    static String getRoot() {
        return root;
    }

}
