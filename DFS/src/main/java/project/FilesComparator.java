package project;

import java.io.*;
import java.sql.*;
import java.util.*;

import static java.lang.System.*;


public class FilesComparator {

    private static final int NUM_THREADS = Runtime.getRuntime().availableProcessors();
    private static final int BATCH_SIZE = 1024;
    private Queue<ArrayList<File>> inputSameSizeFiles;
    private Map<Long, LinkedList<File>> duplicatesMap;

    public FilesComparator(Queue<ArrayList<File>> possibleDuplicates) {

        inputSameSizeFiles = possibleDuplicates;
        
    }


    public void startComparison() {

        if (inputSameSizeFiles.isEmpty()) {
            out.println("No duplicates");
            return;
        }

        duplicatesMap = new HashMap<Long, LinkedList<File>>();

        compareFilesThreaded();
        outputDuplicateFiles();
    }

    /**
     * Method creates additional threads which are used to increase performance
     * Method main waits until each of them has finished comparison
     */
    public void compareFilesThreaded() {

        out.println("Comparison begins...");

        long startTime = currentTimeMillis();

        List<Thread> threads = new ArrayList<Thread>();
        try {
            for (int i = 0; i < NUM_THREADS; ++i) {
                Thread thread = createWorkerThread();
                threads.add(thread);
                thread.start();
            }
        } catch (Exception e) {
            out.println(e.getMessage());
        }

        for (int i = 0; i < NUM_THREADS; ++i) {
            try {
                threads.get(i).join();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        long endTime = currentTimeMillis() - startTime;
        out.println("Comparison Ready !");
        out.println("Time taken : " + (endTime) + " ms");
    }

    /**
     * Method records the result in Database. Each scan has a unique id.
     */
    private void outputDuplicateFiles() {

        //DBWriter.createScanTable();

        int duplicatesCounter = 0;
        int megaByte = 1024*1024;
        //int insertsCounter = 0;
        if (duplicatesMap.isEmpty()) {
            out.println("Duplicate files not found");
        } else {
           //try {
                out.println("Recording to Database...");
                //DBWriter.createDuplicatesTable();
                for (Map.Entry<Long, LinkedList<File>> entry : duplicatesMap.entrySet()) {
                    LinkedList<File> duplicates = entry.getValue();
                    for (File file : duplicates) {
                        duplicatesCounter++;
//                        DBWriter.preparedStatement.setString(1, file.getAbsolutePath());
//                        DBWriter.preparedStatement.setLong(2, DBWriter.scanId);
//                        DBWriter.preparedStatement.addBatch();
//                        if (insertsCounter++ >= BATCH_SIZE) {
//                            DBWriter.preparedStatement.executeBatch();
//                            insertsCounter = 0;
//                        }
//                    }
//                }
//                DBWriter.preparedStatement.executeBatch();
//                DBWriter.dbConnection.commit();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
                        GUI.textArea.append(duplicatesCounter + ")" + "   " + file.getAbsolutePath() + "  "
                                + entry.getKey()/megaByte + "MB" + "\n");
                    }
                    out.println("Possible duplicates: " + duplicatesCounter);
                    //DBWriter.closeConnection();
                }
            //} catch (Exception ex) {}
        }
    }



    /**
     * Each additional thread uses this method to compare files separately from other threads
     * To avoid interruption and ensure data integrity, queue of files placed into synchronized block
     *
     * @return thread
     */
    public Thread createWorkerThread() {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                while (!inputSameSizeFiles.isEmpty()) {
                    ArrayList<File> sameSizeFiles;
                    synchronized (inputSameSizeFiles) {
                        if (!inputSameSizeFiles.isEmpty()) {
                            sameSizeFiles = inputSameSizeFiles.remove();
                        } else {
                            break;
                        }
                    }
                    compareFiles(sameSizeFiles);
                }
            }
        });
    }


    /**
     * Method iterates through files that have same size and invokes method that compares them
     * Identical files are placed into Map.
     *
     * @param files is a ArrayList of files that have same size
     */
    private void compareFiles(ArrayList<File> files) {

        int n = files.size();
        for (int i = 0; i < n - 1; i++) {
            File fileA = files.get(i);
            for (int j = i + 1; j < n; j++) {
                File fileB = files.get(j);
                if (compareFilesFully(fileA, fileB)) {
                    synchronized (duplicatesMap) {
                        addDuplicateFile(duplicatesMap, fileA);
                        addDuplicateFile(duplicatesMap, fileB);
                    }
                }
            }
        }
    }

    /**
     * Method receives file size as scanId and puts it in the appropriate HashSet(creates new one if map doesn't contain)
     * In this map all files are duplicatesMap according to the size
     *
     * @param map  is a container of same files
     * @param file is a duplicate
     */
    private void addDuplicateFile(Map<Long, LinkedList<File>> map, File file) {

        long length = file.length();
        if (map.containsKey(length)) {
            LinkedList<File> sameSize = map.get(length);
            if (!sameSize.contains(file)) {
                sameSize.add(file);
            }
        } else {
            LinkedList<File> sameSize = new LinkedList<File>();
            sameSize.add(file);
            map.put(length, sameSize);
        }

    }

    /**
     * Method fully compares two files with same size block by block. Bytes received via read() method
     * from File Input Stream are compared as byte arrays
     *
     * @param firstPossibleDuplicate  file to compare
     * @param secondPossibleDuplicate file to compare
     * @return false if at least two arrays are not equal or method read () returns different amount of bytes
     */
    public boolean compareFilesFully(File firstPossibleDuplicate, File secondPossibleDuplicate) {

        long fileSize = firstPossibleDuplicate.length(); //secondPossibleDuplicate.length() would be the same

        long bytesCompared = 0;

        int bufferSize = 64*1024;                 //4KB   - 160 sec; 8KB   - 163 sec; 64KB  - 110 sec; 128KB - 123 sec

        if (bufferSize > fileSize) {
            bufferSize = (int) fileSize;
        }

        try {

            InputStream firstInputStream = new FileInputStream(firstPossibleDuplicate);
            InputStream secondInputStream = new FileInputStream(secondPossibleDuplicate);

            byte[] firstArray = new byte[bufferSize];
            byte[] secondArray = new byte[bufferSize];

            while (bytesCompared < fileSize) {

                int firstReadedAmount = firstInputStream.read(firstArray, 0, bufferSize);
                int secondReadedAmount = secondInputStream.read(secondArray, 0, bufferSize);

                if (firstReadedAmount != secondReadedAmount || !Arrays.equals(firstArray, secondArray)) {
                    firstInputStream.close();
                    secondInputStream.close();
                    return false;
                }
                bytesCompared = bytesCompared + firstReadedAmount; //secondReadedAmount would be the same
            }

            firstInputStream.close();
            secondInputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

}


