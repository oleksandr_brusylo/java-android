package project;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static java.lang.System.out;

public class TreeBuilder {

    private Queue<File> dirsQueue = new LinkedList<File>();
    private Map<Long, ArrayList<File>> inputFiles = new HashMap<Long, ArrayList<File>>();
    private Queue<ArrayList<File>> filesQueue = new LinkedList<ArrayList<File>>();
    private String root;

    public TreeBuilder(String root) {
        this.root = root;
    }


    /**
     * Methods builds a map with scanId of file size and ArrayList<File> as value
     * To avoid recursion file system traversed using queue
     */
    public void buildFileTree() throws IOException {

        File root = new File(this.root);

        if (root.isDirectory())
            dirsQueue.add(root);
        else {
            throw new IOException("Not a directory");
        }

        while (!dirsQueue.isEmpty()) {
            File[] content = dirsQueue.remove().listFiles();
            if (content != null) {
                for (File file : content) {
                    if (file.isDirectory()) {
                        dirsQueue.add(file);
                    } else {
                        if (file.length() > 0) {
                            addFileToMap(inputFiles, file);
                        }
                    }
                }
            }
        }
        prepareFilesQueue();
    }

    /**
     * Method receives file size as scanId and puts it in the appropriate ArrayList(creates new one if map doesn't contain)
     *
     * @param map  is a container of all files founded in directories and subdirectories
     * @param file will be added to one of ArrayLists
     */
    public void addFileToMap(Map<Long, ArrayList<File>> map, File file) {

        long length = file.length();

        if (map.containsKey(length)) {
            ArrayList<File> sameSize = map.get(length);
            sameSize.add(file);
        } else {
            ArrayList<File> sameSize = new ArrayList<File>();
            sameSize.add(file);
            map.put(length, sameSize);
        }
    }

    /**
     * Iterates through map of all ArrayLists and adds Arrays with size greater than 1 to queue
     */
    public void prepareFilesQueue() {

        if (inputFiles.isEmpty()) {
            out.println("Files not found");
            return;
        }
        for (Map.Entry<Long, ArrayList<File>> entry : inputFiles.entrySet()) {
            ArrayList<File> sameSizeFiles = entry.getValue();

            if (sameSizeFiles.size() > 1) {
                filesQueue.add(sameSizeFiles);
            }
        }
        out.println("Files Queue Ready!");
    }

    public Queue<ArrayList<File>> getFiles() {
        return filesQueue;
    }
}
